﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Server.Global
{
    public class ErrorCode
    {

        public enum Status : sbyte
        {
	        Error_Critical,
	        Error_Regular,
	        Warning,
            Question,
	        Info,
	        Success
        };
        
        private static Dictionary<Status, Color> status_table = new Dictionary<Status, Color>()
        {
            
            {Status.Error_Critical,  Properties.ColorDefinition.Default.ErrorCode_ErrorCritical},
            { Status.Error_Regular, Properties.ColorDefinition.Default.ErrorCode_ErrorRegular},
            { Status.Warning,  Properties.ColorDefinition.Default.ErrorCode_Warning},
            { Status.Question, Properties.ColorDefinition.Default.ErrorCode_Question},
            { Status.Info, Properties.ColorDefinition.Default.ErrorCode_Info},
            { Status.Success, Properties.ColorDefinition.Default.ErrorCode_Success},
        };
        
        public virtual Status status { get; set; }
        public virtual string msg_text { get; set; }
        public virtual Color msg_color { get; set; }


        public ErrorCode(Status i_status)
        {
            this.status = i_status;
            this.msg_color = status_table[i_status];
            msg_text = Constants.UNKNOWN;
        }

        public ErrorCode(Status i_status, String i_msg)
        {
            this.status = i_status;
            this.msg_color = status_table[i_status];
            msg_text = Constants.UNKNOWN;
        }


        /// <summary>
        /// Equals for an ErrorCode object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>bool</returns>
        public virtual bool Equals(ErrorCode errorcode)
        {
            // If parameter is null return false.
            if (errorcode == null)
            {
                return false;
            }

            return status.Equals(errorcode.status);
        }
    }
}
