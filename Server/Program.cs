﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Utils.DB;
using Server.Objects.TableObjects.Reference;

namespace Server
{

    class Program
    {
        //private static readonly ILog Log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            DBConnect dbconnect = new DBConnect();
            if (dbconnect.OpenConnection() == true)
            {
                TBRole.Instance.loadTableToCache(dbconnect);
                dbconnect.CloseConnection();
            }


            //dbconnect.Insert();

        }
    }
}
