﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Objects.BusinessObjects;
using Server.Global;
using Server.Objects.BusinessObjects.ReferenceBO;

namespace Server.Objects.BOServices.BORefrenceServices
{
    
    // TODO: RoleServices To move this class to Role Table Class


    class RoleServices
    {
        /*
        private static RoleServices instance = null;
        private static readonly object padlock = new object();
        private static Dictionary<string, RoleObject> role_table = null;

        /// <summary>
        /// Default Constructor - Private
        /// </summary>
        private RoleServices() { ;}

        /// <summary>
        /// Singelton
        /// </summary>
        public static RoleServices Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new RoleServices();
                    }
                    return instance;
                }
            }
        }

        private static void init()
        {
            //Add all Entries
            //loadFromtable
            role_table = new Dictionary<string, RoleObject>();
            role_table.Add(Constants.UNKNOWN, new RoleObject(Constants.UNKNOWN, 0, 0));
            role_table.Add(Constants.DEFAULT, new RoleObject(Constants.DEFAULT, 5, 5));
            role_table.Add("Viewer", new RoleObject("Viewer", 10, 0));
            role_table.Add("Installer", new RoleObject("Installer", 10, 8));
            role_table.Add("Admin", new RoleObject("Admin", 10, 10)); 
        }

        
        // TODO: RoleServices Implamant loadFromTable
        private static void loadFromTable()     
        {
            //string query = "SELECT * FROM tableinfo";
            string query = String.Format(CommonSQL.SELECT_STAR, "tbr_role");

            //Create a list to store the result
            List<string>[] list = new List<string>[3];
            list[0] = new List<string>();
            list[1] = new List<string>();
            list[2] = new List<string>();

            //Open connection
            if (this.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    list[0].Add(dataReader["NAME"] + "");
                    list[1].Add(dataReader["READ_PERMISSION"] + "");
                    list[2].Add(dataReader["WRITE_PERMISSION"] + "");
                }

                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();

                //return list to be displayed
                return list;
            }
            else
            {
                return list;
            }
        }
        }


        public RoleObject getRole(string role_name)
        {
            RoleObject value;

            bool hasValue = role_table.TryGetValue(role_name, out value);
            if (hasValue)
            {
                return value;
            }

            return role_table[Constants.UNKNOWN];
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns>Role Tabele as a String</returns>
        public String toString()
        {
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.Append("RoleServices::Dictionary");
            strBuilder.Append(System.Environment.NewLine);

            foreach (RoleObject role in role_table.Values)
            {
                strBuilder.Append(role.ToString());
                strBuilder.Append(System.Environment.NewLine);
            }

            return strBuilder.ToString();
        }
     * */
    
    }
         
}
