﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Objects.BusinessObjects.ReferenceBO;
using Server.Global;
using Server.Utils.DB.SQL;
using Server.Utils.DB;
using MySql.Data.MySqlClient;
using System.Data;

namespace Server.Objects.TableObjects.Reference
{
    class TBRole : TableInterfaceRefrence
    {
        private static TBRole instance = null;
        private static readonly object padlock = new object();
        private static Dictionary<string, RoleObject> role_table = null;

        /// <summary>
        /// Default Constructor - Private
        /// </summary>
        private TBRole() { ;}

        /// <summary>
        /// Singelton
        /// </summary>
        public static TBRole Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new TBRole();
                    }
                    return instance;
                }
            }
        }

        /// <summary>
        /// getRole by Role Name
        /// </summary>
        /// <param name="role_name"></param>
        /// <returns></returns>
        public RoleObject getRole(string role_name)
        {
            RoleObject value;

            bool hasValue = role_table.TryGetValue(role_name, out value);
            if (hasValue)
            {
                return value;
            }

            return role_table[Constants.UNKNOWN];
        }


        /// <summary>
        /// toString
        /// </summary>
        /// <returns>Role Tabele as a String</returns>
        public String toString()
        {
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.Append("TBRole::Dictionary");
            strBuilder.Append(System.Environment.NewLine);

            foreach (RoleObject role in role_table.Values)
            {
                strBuilder.Append(role.ToString());
                strBuilder.Append(System.Environment.NewLine);
            }

            return strBuilder.ToString();
        }

        /// <summary>
        /// loadTableToCache role_table
        /// this method DO NOT response to open\close new connection!!!
        /// </summary>
        /// <returns></returns>
        public ErrorCode loadTableToCache(DBConnect dbConnect)
        {
            role_table = new Dictionary<string, RoleObject>();
            string query = String.Format(CommonSQL.SELECT_STAR, "tbr_role");

            //Open connection

            if (dbConnect != null && dbConnect.connection.State == ConnectionState.Open)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, dbConnect.connection);
                
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    //byte[] f = (byte[])dataReader["READ_PERMISSION"];
                    //Console.WriteLine(f);
                    
                    role_table.Add((string)dataReader["NAME"],
                        new RoleObject(dataReader["NAME"] + "",
                            ((short)dataReader["READ_PERMISSION"]), 
                             ((short)dataReader["WRITE_PERMISSION"])));
                     
                }

                //TODO Print to the log instead of Console
                //TODO Add msg to ErrorCode
                Console.WriteLine(toString());
                return new ErrorCode(ErrorCode.Status.Success);
            }
            else
            {
                //TODO Print to the log instead of Console
                //TODO Add msg to ErrorCode
                return new ErrorCode(ErrorCode.Status.Error_Critical);
            }

        }

        /// <summary>
        ///Clear table
        ///should use only in case there is a new data in the table
        /// </summary>
        public void clearTableFromCache()
        {
            role_table.Clear();
        }

        //TODO - To Implamant method
        /// <summary>
        /// updateSingleEntry
        /// this method should be used only on case that table was updated
        /// </summary>
        /// <returns></returns>
        public ErrorCode updateSingleEntry()
        {
            return null;
        }
    }
}
