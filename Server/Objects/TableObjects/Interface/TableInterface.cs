﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Global;

namespace Server.Objects.TableObjects
{

    public interface TableInterface
    {
        String toString();
    }
}
