﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Global;
using Server.Utils.DB;

namespace Server.Objects.TableObjects
{
    interface TableInterfaceRefrence : TableInterface
    {
        ErrorCode loadTableToCache(DBConnect dbConnect);
        void clearTableFromCache();
        ErrorCode updateSingleEntry();
    }
}
