﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Global;
using Server.Objects.BusinessObjects.ReferenceBO;

namespace Server.Objects.BusinessObjects
{
	class UserObject
	{
        //public static readonly string SEQUANCE_NAME = "UserObject";
        //private int? user_object_seqaunce;
        //public virtual int unique_id { get; set; }

        public virtual string name { get; set; }
        public virtual string pass { get; set; }
        public virtual DateTime effective_date_start { get; set; }
        public virtual DateTime effective_date_end { get; set; }
        public virtual RoleObject role { get; set; }

        public UserObject(string i_user_name ,string i_user_pass)
        {
            name = i_user_name;
            pass = i_user_pass;
            role = new RoleObject(Constants.DEFAULT);
        }


        public UserObject(string i_user_name, string i_user_pass,string role_Name)
        {
            name = i_user_name;
            pass = i_user_pass;
            role = new RoleObject(role_Name);
        }

        /// <summary>
        /// Equals for an UserAuthorization object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>bool</returns>
        public virtual bool Equals(UserObject userObject)
        {
            // If parameter is null return false.
            if (userObject == null)
            {
                return false;
            }

            return name.Equals(userObject.name);
        }

        /// <summary>
        /// GetHashCode
        /// </summary>
        /// <returns>uniqe code of name</returns>
        public override int GetHashCode()
        {
            return name.GetHashCode();
        }

        /// <summary>
        /// ToString
        /// </summary>
        /// <returns>Role details as String</returns>
        public override string ToString()
        {
            StringBuilder strBuilder = new StringBuilder();

            strBuilder.Append("UserObject: User Name = ")
                .Append(name)
                .Append(" , Effective Date Start = ")
                .Append(effective_date_start)
                .Append(" , Effective Date End= ")
                .Append(effective_date_start)
                .Append(" , Role = ")
                .Append(role);

            return strBuilder.ToString();
        }

        /// <summary>
        /// Check user Effective date
        /// </summary>
        /// <returns></returns>
        public virtual bool isUserEffectiveDateValid()
        {
            return DateTime.Today <= effective_date_start;
        }

        /*
        /// <summary>
        /// Check that user bane is valid
        /// Check that user password is currect
        /// Cehck that user has valid effective date
        /// </summary>
        /// <param name="i_userName"></param>
        /// <param name="i_UserPass"></param>
        /// <returns></returns>
        public virtual ErrorCode isUserAuthorized(String i_UserPass)
        {
            if (String.IsNullOrEmpty(user_name))
            {
                return new ErrorCode(TableMsg.Instance.getMsgByName(MsgNames.userNotFound));
            }

            if(String.IsNullOrEmpty(i_UserPass) || !i_UserPass.Equals(user_pass))
            {
                return new ErrorCode(TableMsg.Instance.getMsgByName(MsgNames.userWrongPass));
            }

            if(!isUserEffectiveDateValid())
            {
                return new ErrorCode(TableMsg.Instance.getMsgByName(MsgNames.userExpired));
            }

            return new ErrorCode();   
        }
         * */

    
	}
}
