﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Objects.BOServices.BORefrenceServices;
using Server.Objects.TableObjects.Reference;

namespace Server.Objects.BusinessObjects.ReferenceBO
{
    class RoleObject
    {
        private readonly string name;
        private readonly short read_permission;
        private readonly short write_permission;

        public RoleObject(string i_role_name)
        {
            TBRole tbrole = TBRole.Instance;
            new RoleObject(tbrole.getRole(i_role_name));
        }


        public RoleObject(string i_name, short i_read_permission, short i_write_permission)
        {
            this.name = i_name;
            this.read_permission = i_read_permission;
            this.write_permission = i_write_permission;
        }

        private RoleObject(RoleObject i_roleObject)
        {
            this.name = i_roleObject.name;
            this.read_permission = i_roleObject.read_permission;
            this.write_permission = i_roleObject.write_permission;
        }

        /// <summary>
        /// Equals for an Role object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>bool</returns>
        public virtual bool Equals(RoleObject role)
        {
            // If parameter is null return false.
            if (role == null)
            {
                return false;
            }

            return name.Equals(role.name);
        }

        /// <summary>
        /// GetHashCode
        /// </summary>
        /// <returns>uniqe code of name</returns>
        public override int GetHashCode()
        {
            return name.GetHashCode();
        }

        /// <summary>
        /// ToString
        /// </summary>
        /// <returns>Role details as String</returns>
        public override string ToString()
        {
            StringBuilder strBuilder = new StringBuilder();

            strBuilder.Append("Role: Name = ")
                .Append(name)
                .Append(" , Read = ")
                .Append(read_permission)
                .Append(" , Write = ")
                .Append(write_permission);

            return strBuilder.ToString();
        }
    }

}
