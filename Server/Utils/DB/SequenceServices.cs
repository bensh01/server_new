﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Server.Global;
using Server.Objects.BusinessObjects;

namespace Server.Utils.DB
{
    class SequenceServices
    {
        private static SequenceServices instance = null;
        private static readonly object padlock = new object();
        private static Dictionary<string, string> sequence_table = null;

        /// <summary>
        /// Default Constructor - Private
        /// </summary>
        private SequenceServices() 
        {
            init();
        }

        /// <summary>
        /// Singelton
        /// </summary>
        public static SequenceServices Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new SequenceServices();
                    }
                    return instance;
                }
            }
        }

        private static void init()
        {
            sequence_table = new Dictionary<string, string>();
            //Add all Entries
                //User Object Seqaunce
        }

        public static string getSequence(string sequance_name)
        {
            string retVal = Constants.UNKNOWN;
            string value;
            
            bool hasValue = sequence_table.TryGetValue(sequance_name,out value);
            if (hasValue)
            {
                retVal = value;
            }

            return retVal;
        }


        public static void setSequence(string sequance_name,string sequance_new_value)
        {
            string val;
            if (sequence_table.TryGetValue(sequance_name, out val))
            {
                sequence_table[sequance_name] = sequance_new_value;
            }
        }
    }
}
