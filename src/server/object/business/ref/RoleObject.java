package server.object.business.ref;

import server.object.table.ref.RoleTable;
import server.utils.constants.Common;

public class RoleObject
{
	private String name;
    private short read_permission;
    private short write_permission;
    
    /**
     * Start Constructors,get,set 
     */
    
    public RoleObject()
    {
    	new RoleObject(Common.UNKNOWN);
    }
    
    public RoleObject(String i_name,short i_read_permission,short i_write_permission)
    {
    	name = i_name;
    	read_permission = i_read_permission;
    	write_permission = i_write_permission;
    }
    
    public RoleObject(String i_name)
    {
    	RoleTable rb  = RoleTable.getInstance();
    	rb.getSingleEntry(i_name);
    	new RoleObject((RoleObject)rb.getSingleEntry(i_name));
    }
    
    public RoleObject(RoleObject i_roleObject)
    {
    	name = i_roleObject.name;
    	read_permission = i_roleObject.read_permission;
    	write_permission = i_roleObject.write_permission;
    }
    
    public String getName() {
		return name;
	}
	public short getRead_permission() {
		return read_permission;
	}
	public short getWrite_permission() {
		return write_permission;
	}
	
    /**
     * End Constructors,get,set 
     */

	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer("Role : ");
		sb.append("RoleName : ").append(name);
		sb.append(" , Read Permission : ").append(read_permission);
		sb.append(" , Write Permission : ").append(write_permission);

		return sb.toString();
	}
	
}
