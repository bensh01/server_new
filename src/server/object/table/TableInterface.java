package server.object.table;

public interface TableInterface 
{
	String getTableName();
	String tableToString();
}
