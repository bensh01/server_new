package server.object.table;

import java.sql.SQLException;
import java.sql.ResultSet;

import server.utils.errors.ErrorCode;

public interface  TableInterfaceRef extends TableInterface
{
    ErrorCode loadTableToCache(ResultSet resultSet) throws SQLException; 
    public void  clearTableFromCache();
    ErrorCode updateSingleEntry(String key);
    Object getSingleEntry(String key);
}
