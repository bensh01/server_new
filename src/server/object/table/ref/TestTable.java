package server.object.table.ref;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import server.object.table.TableInterfaceRef;
import server.utils.errors.ErrorCode;

public class TestTable implements TableInterfaceRef {

	ArrayList<String> entries;
	private static TestTable instance = null;
	static Object  padlock = new Object();

	
	private TestTable()
	{
		entries = new ArrayList<String>();
	}
	
    /**
     * Singleton - get instance
     */
    public static TestTable getInstance()
    {	 
    	if (instance == null) 
    	{
    		synchronized (padlock) 
   		 	{
    			instance = new TestTable();
            }
        }
   	 	return instance;
    }
	
    
	@Override
	public String getTableName() {
		// TODO Auto-generated method stub
		return "tbr_test";
	}

	@Override
	public String tableToString() {
		
		StringBuffer sb = new StringBuffer(this.getTableName());
		sb.append(System.lineSeparator());
		
		for (String  entry : entries) 
		{
			sb.append(entry);
			sb.append(System.lineSeparator());
		}
		
		return sb.toString();
	}

	@Override
	public ErrorCode loadTableToCache(ResultSet resultSet) throws SQLException 
	{
		String A,B,C;
		while (resultSet.next()) 
		{
			A = resultSet.getString("A");
	        B = resultSet.getString("B");
	        C = resultSet.getString("C");
	        entries.add(A + ", " + B + " , " + C);    
		}
		
		System.out.println(tableToString());
		return  ErrorCode.Success;

	}

	@Override
	public void clearTableFromCache() {
		// TODO Auto-generated method stub

	}

	@Override
	public ErrorCode updateSingleEntry(String key) {
		// TODO Auto-generated method stub
		return  ErrorCode.Success;
	}

	@Override
	public Object getSingleEntry(String key) {
		// TODO Auto-generated method stub
		return null;
	}

}
