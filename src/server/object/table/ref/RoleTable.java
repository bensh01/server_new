package server.object.table.ref;

import java.sql.SQLException;
import java.util.HashMap;
import java.sql.ResultSet;

import server.object.business.ref.RoleObject;
import server.object.table.TableInterfaceRef;
import server.utils.constants.Common;
import server.utils.errors.ErrorCode;

public class RoleTable implements TableInterfaceRef {

	private static HashMap<String, RoleObject> role_tbale_map = null;
	private static RoleTable instance = null;
	static Object  padlock = new Object();
	
	
	private RoleTable()
	{
		role_tbale_map = new HashMap<String, RoleObject>();
	}
	
    /**
     * Singleton - get instance
     */
    public static RoleTable getInstance()
    {	 
    	if (instance == null) 
    	{
    		synchronized (padlock) 
   		 	{
    			instance = new RoleTable();
            }
        }
   	 	return instance;
    }
	
    @Override
	public String getTableName() 
	{
		return "tbr_role";
	}
	
    @Override
	public ErrorCode loadTableToCache(ResultSet resultSet) throws SQLException 
	{	
		clearTableFromCache();
		
		synchronized (padlock) 
		{
			String roleName;
			short readPermission,writePermission;
			while (resultSet.next()) 
			{
				roleName = resultSet.getString("NAME");
		        readPermission = resultSet.getShort("READ_PERMISSION");
		        writePermission = resultSet.getShort("WRITE_PERMISSION");
		        
		        role_tbale_map.put(roleName, new RoleObject(roleName,readPermission,writePermission));
			}
			
			System.out.println(tableToString());
		}
		
		return  ErrorCode.Success;
	}


	public void clearTableFromCache()
	{
		synchronized (padlock) 
		{
			role_tbale_map.clear();
		}
	}

	@Override
	public String tableToString() 
	{
		StringBuffer sb = new StringBuffer(this.getTableName());
		sb.append(System.lineSeparator());
		
		for (RoleObject  rb : role_tbale_map.values()) 
		{
			sb.append(rb.toString());
			sb.append(System.lineSeparator());
		}
		
		return sb.toString();
	}

	@Override
	public ErrorCode updateSingleEntry(String key) 
	{
		// TODO updateSingleEntry Auto-generated method stub
		return  ErrorCode.Success;
		
	}

	@Override
	public Object getSingleEntry(String key) 
	{
		if(role_tbale_map.containsKey(key))
		{
			return role_tbale_map.get(key);
		}
		else
		{
			return role_tbale_map.get(Common.DEFAULT);
		}
	}

}
