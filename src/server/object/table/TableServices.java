package server.object.table;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import server.configuration.ConfigurationUtils;
import server.object.table.ref.RoleTable;
import server.object.table.ref.TestTable;
import server.utils.constants.CommonSQL;
import server.utils.errors.ErrorCode;


public class TableServices 
{
	private ArrayList <TableInterfaceRef> tableList = null;
	private static TableServices instance = null;
	static Object  padlock = new Object();
	private Connection connect = null;
	private Statement statement = null;
	private ResultSet rs  = null;
			  
	private TableServices()
	{
		tableList = new  ArrayList <TableInterfaceRef>() ;
		tableList.add(TestTable.getInstance());
		tableList.add(RoleTable.getInstance());
	}
	
    /**
     * Singleton - get instance
     */
    public static TableServices getInstance()
    {	 
    	if (instance == null) 
    	{
    		synchronized (padlock) 
   		 	{
    			instance = new TableServices();
            }
        }
   	 	return instance;
    }
	
	//TODO Change return value to ErrorCode
	public void loadAllRefTableToCache() throws SQLException
	{
		if(tableList == null && tableList.isEmpty())
		{
			return;
		}
		
		try
		{
			createConnection();
			
			for(TableInterfaceRef table: tableList)
			{
				rs = statement.executeQuery(prapreSelectSQLForSingleTable(table));
				table.loadTableToCache(rs);
			}
		}
		finally
		{
			closeConnection();
		}
	}
	
	public String  prapreSelectSQLForSingleTable(TableInterfaceRef table)
	{
		StringBuffer query = new StringBuffer(CommonSQL.SELECT_STAR);
		query.append(table.getTableName());
		return query.toString();
	}
	
	
	//TODO Change return value to ErrorCode
	public void  createConnection()
	{
    	ConfigurationUtils confService = ConfigurationUtils.getInstance();
    	Properties dbConfigFile = confService.getFileConfiguration("db_connection.properties");
    	
    	try 
    	{
        	//This will load the MySQL driver, each DB has its own driver
    		Class.forName("com.mysql.jdbc.Driver");
    		// Setup the connection with the DB
    		connect = DriverManager.getConnection
    				(dbConfigFile.getProperty("db.url"),
    						dbConfigFile.getProperty("db.user"),
    						dbConfigFile.getProperty("db.password"));

    		// Statements allow to issue SQL queries to the database
    		statement = connect.createStatement();	
		} 
    	catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	catch (SQLException e)
    	{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 	
	}
	
	
	private ErrorCode closeConnection()
	{
	    try 
	    {
	        if (rs != null)
	        {
	          rs.close();
	        }

	        if (statement != null)
	        {	
	          statement.close();
	        }

	        if (connect != null)
	        {
	          connect.close();
	        }
	    }
	    catch (Exception e) 
	    {
	    	//TODO Add appropriate  Message
	    	return ErrorCode.Error;
	    	
	    }
	    return ErrorCode.Success;
	}
	
	
}
