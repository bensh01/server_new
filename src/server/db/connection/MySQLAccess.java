package server.db.connection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


import java.util.Properties;

import server.configuration.ConfigurationUtils;
import server.object.table.TableServices;
import server.object.table.ref.RoleTable;
import server.object.table.ref.TestTable;

public class MySQLAccess 
{
  private Connection connect = null;
  private Statement statement = null;
  private PreparedStatement preparedStatement = null;
  private ResultSet resultSet = null;

  public void readDataBase() throws Exception 
  {
    try 
    {
    	ConfigurationUtils confService = ConfigurationUtils.getInstance();
    	Properties dbConfigFile = confService.getFileConfiguration("db_connection.properties");
    	
    	//This will load the MySQL driver, each DB has its own driver
    	Class.forName("com.mysql.jdbc.Driver");
    	// Setup the connection with the DB
    	connect = DriverManager.getConnection
    			(dbConfigFile.getProperty("db.url"),
    					dbConfigFile.getProperty("db.user"),
    					dbConfigFile.getProperty("db.password"));

    	// Statements allow to issue SQL queries to the database
    	statement = connect.createStatement();
    	// Result set get the result of the SQL query
    	TableServices ts = TableServices.getInstance();
    	
    	resultSet = statement.executeQuery("select * from server.tbr_test");
    	TestTable roleTable = TestTable.getInstance();
    	
    	roleTable.loadTableToCache(resultSet);
    	//writeResultSet(resultSet);

	      /*
	      // PreparedStatements can use variables and are more efficient
	      preparedStatement = connect
	          .prepareStatement("insert into  feedback.comments values (default, ?, ?, ?, ? , ?, ?)");
	      // "myuser, webpage, datum, summery, COMMENTS from feedback.comments");
	      // Parameters start with 1
	      preparedStatement.setString(1, "Test");
	      preparedStatement.setString(2, "TestEmail");
	      preparedStatement.setString(3, "TestWebpage");
	      preparedStatement.setDate(4, new java.sql.Date(2009, 12, 11));
	      preparedStatement.setString(5, "TestSummary");
	      preparedStatement.setString(6, "TestComment");
	      preparedStatement.executeUpdate();
	
	      preparedStatement = connect
	          .prepareStatement("SELECT myuser, webpage, datum, summery, COMMENTS from feedback.comments");
	      resultSet = preparedStatement.executeQuery();
	      writeResultSet(resultSet);
	
	      // Remove again the insert comment
	      preparedStatement = connect
	      .prepareStatement("delete from feedback.comments where myuser= ? ; ");
	      preparedStatement.setString(1, "Test");
	      preparedStatement.executeUpdate();
	      
	      resultSet = statement
	      .executeQuery("select * from feedback.comments");
	      writeMetaData(resultSet);
	      */
      
    } catch (Exception e) {
      throw e;
    } finally {
      close();
    }

  }

  private void writeResultSet(ResultSet resultSet) throws SQLException {
    // ResultSet is initially before the first data set
    while (resultSet.next()) {
      // It is possible to get the columns via name
      // also possible to get the columns via the column number
      // which starts at 1
      // e.g. resultSet.getSTring(2);
      String roleName = resultSet.getString("NAME");
      String readPermission = resultSet.getString("READ_PERMISSION");
      String writePermission = resultSet.getString("WRITE_PERMISSION");
      System.out.println("roleName: " + roleName);
      System.out.println("readPermission: " + readPermission);
      System.out.println("writePermission: " + writePermission);
    }
  }

  // You need to close the resultSet
  private void close() {
    try {
      if (resultSet != null) {
        resultSet.close();
      }

      if (statement != null) {
        statement.close();
      }

      if (connect != null) {
        connect.close();
      }
    } catch (Exception e) {

    }
  }

} 