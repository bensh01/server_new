package server.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;

public class ConfigurationUtils 
{
	
	 private static ConfigurationUtils instance = null;
     private static Object  padlock = new Object();
     
     //TODO add the option to load configuration files to cache
    //private static HashMap<String, Configuration> configurationMap = null;

     
     /**
      * Default Constructor - Private
      */
     private ConfigurationUtils() 
     {
         init();
     }

     
     /**
      * Singleton - get instance
      */
     public static ConfigurationUtils getInstance()
     {	 
    	 if (instance == null) 
    	 {
             synchronized (padlock) 
             {
                     instance = new ConfigurationUtils();
             }
         }
    	 return instance;
     }

     private static void init()
     {
    	 //configurationMap = new HashMap<String,Configuration>();
    	 
    	 //init db_connection.properties
    	 //configurationMap.put("db_connection.properties", getFileConfiguration("db_connection.properties")); 	 
     }

	
     //Using Apache Jars - NOT IN USED !!!
	public Configuration getFileConfigurationAppach(String fileName)
	{
		Configuration config  = null;
		Parameters params = new Parameters();
		@SuppressWarnings({ "rawtypes", "unchecked" })
		FileBasedConfigurationBuilder builder =
		    new FileBasedConfigurationBuilder(PropertiesConfiguration.class)
		    .configure(params.properties()
		        .setFileName(fileName));
		try
		{
		    config = (Configuration) builder.getConfiguration();
		}
		catch(ConfigurationException cex)
		{
			
		    // loading of the configuration file failed
		}
		return config;
	}
	
	
	public Properties getFileConfiguration(String fileName)
	{
		Properties configFile = new Properties();
		try 
		{
			InputStream in = getClass().getResourceAsStream(fileName);
			configFile.load(in);
			in.close();
			return configFile;
		} 
		catch (IOException e) 
		{
			//TODO add log 
			e.printStackTrace();
		}
		return configFile;
	}
	
}
