package server.utils.errors;

public enum ErrorCode 
{
	
	/**
	 * General Error Codes
	 * Critical : 1-100 - Start with C_XXX
	 * Error    : 101-200 - Start with E_XXX
	 * Warning  : 201-300 - Start with W_XXX
	 * Question : 301-400 - Start with Q_XXX
	 * Info		: 401-500 - Start with I_XXX
	 * Success	: 801+ - Start with S_XXX
	 */
	Critical (1, "A Critical error has occured."),
	Error (101, "A Error error has occured."),
	Warning (201, "A Warning msg."),
	Question (301, "A Question been sent"),
	Info (401, "A Info msg sent ."),
	Success (801, "Operation Success");
	
	/**
	 * Specific ErrorCode
	 */
	//C_DATABASE(0,0, "A database error has occured."),
	//E_DUPLICATE_USER(0,1, "This user already exists."),

	  
	private final int code;
	private final String description;
	  
	private ErrorCode(int code, String description)
	{
		this.code = code;
		this.description = description;    
	}

	public int getCode() 
	{
		return code;
	}
	
	public String getDescription() 
	{
		return description;
	}
	  
}
